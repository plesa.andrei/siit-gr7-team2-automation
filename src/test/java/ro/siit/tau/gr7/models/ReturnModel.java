package ro.siit.tau.gr7.models;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ReturnModel {

    private String firstName ;//
    private String lastName ;//
    private String email;//
    private String telephone;//

    private String orderID;//
    private String orderDate;

    private String productName;//
    private String productCode;//
    private String quantity;
    private String valueReasonBox;//
    private String details;

    private String firstNameError;//
    private String lastNameError ;//
    private String emailError;//
    private String telephoneError;//
    private String orderIDError;//
    private String productNameError;//
    private String productCodeError;//
    private String valueReasonBoxError;//

    private String typeOfTest;

    //getters

    public String getFirstName() {return firstName;}
    public String getLastName() {return lastName;}
    public String getEmail() {return email;}
    public String getTelephone() {return telephone;}

    public String getOrderID() {return orderID;}
    public String getOrderDate() {return orderDate;}

    public String getProductName() {return productName;}
    public String getProductCode() {return productCode;}
    public String getQuantity() {return quantity;}
    public String getValueReasonBox() {return valueReasonBox;}
    public String getDetails() {return details;}

    public String getFirstNameError() {return firstNameError;}
    public String getLastNameError() {return lastNameError;}
    public String getEmailError() {return emailError;}
    public String getTelephoneError() {return telephoneError;}

    public String getOrderIDError() {return orderIDError;}
    public String getProductNameError() {return productNameError;}
    public String getProductCodeError() {return productCodeError;}
    public String getValueReasonBoxError() {return valueReasonBoxError;}

    public String getTypeOfTest() {return typeOfTest;}

    //setters

    @XmlElement
    public void setFirstName(String firstName) {this.firstName = firstName;}
    @XmlElement
    public void setLastName(String lastName) {this.lastName = lastName;}
    @XmlElement
    public void setEmail(String email) {this.email = email;}
    @XmlElement
    public void setTelephone(String telephone) {this.telephone = telephone;}

    @XmlElement
    public void setOrderID(String orderID) {this.orderID = orderID;}
    @XmlElement
    public void setOrderDate(String orderDate) {this.orderDate = orderDate;}

    @XmlElement
    public void setProductName(String productName) {this.productName = productName;}
    @XmlElement
    public void setProductCode(String productCode) {this.productCode = productCode;}
    @XmlElement
    public void setQuantity(String quantity) {this.quantity = quantity;}
    @XmlElement
    public void setValueReasonBox(String valueReasonBox) {this.valueReasonBox = valueReasonBox;}
    @XmlElement
    public void setDetails(String details) {this.details = details;}

    @XmlElement
    public void setFirstNameError(String firstNameError) {this.firstNameError = firstNameError;}
    @XmlElement
    public void setLastNameError(String lastNameError) {this.lastNameError = lastNameError;}
    @XmlElement
    public void setEmailError(String emailError) {this.emailError = emailError;}
    @XmlElement
    public void setTelephoneError(String telephoneError) {this.telephoneError = telephoneError;}
    @XmlElement
    public void setOrderIDError(String orderIDError) {this.orderIDError = orderIDError;}

    @XmlElement
    public void setProductNameError(String productNameError) {this.productNameError = productNameError;}
    @XmlElement
    public void setProductCodeError(String productCodeError) {this.productCodeError = productCodeError;}
    @XmlElement
    public void setValueReasonBoxError(String valueReasonBoxError) {this.valueReasonBoxError = valueReasonBoxError;}

    @XmlElement
    public void setTypeOfTest(String typeOfTest) {this.typeOfTest = typeOfTest;}

    public boolean expectSuccessfulRegister() {

        if (!this.getFirstNameError().trim().equalsIgnoreCase("")) {return false;}

        if (!this.getLastNameError().trim().equalsIgnoreCase("")) {return false;}

        if (!this.getEmailError().trim().equalsIgnoreCase("")) {return false;}

        if (!this.getTelephoneError().trim().equalsIgnoreCase("")) {return false;}

        if (!this.getOrderIDError().trim().equalsIgnoreCase("")) {return false;}

        if (!this.getProductNameError().trim().equalsIgnoreCase("")) {return false;}

        if (!this.getProductCodeError().trim().equalsIgnoreCase("")) {return false;}

        if (!this.getValueReasonBoxError().trim().equalsIgnoreCase("")) {return false;}

        return true;
    }
}