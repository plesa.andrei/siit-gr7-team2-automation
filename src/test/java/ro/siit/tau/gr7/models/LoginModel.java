package ro.siit.tau.gr7.models;


import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
    public class LoginModel {

        private String email;
        private String password;
        private String emailError;

        private String typeOfTest;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmailError() {
        return emailError;
    }

    public void setEmailError(String emailError) {
        this.emailError = emailError;
    }

    public String getTypeOfTest() {return typeOfTest;}

    public void setTypeOfTest(String typeOfTest) {
        this.typeOfTest = typeOfTest;
    }
}
