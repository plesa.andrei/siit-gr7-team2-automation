package ro.siit.tau.gr7.models;

public class UserModel {
    String firstName;
    String lastName;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;

    }
    public String toString()
    {
        return String.format("u: %s, p: %s", this.firstName, this.lastName);
    }
}
