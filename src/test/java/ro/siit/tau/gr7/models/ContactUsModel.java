package ro.siit.tau.gr7.models;

public class ContactUsModel {
    private String yourName;
    private String emailAddress;
    private String enquiry;

    private String yourNameError;
    private String emailError;
    private String enquiryError;

    private String typeOfTest;

    public String getYourName() {
        return yourName;
    }

    public void setYourName(String yourName) {
        this.yourName = yourName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getEnquiry() {
        return enquiry;
    }

    public void setEnquiry(String enquiry) {
        this.enquiry = enquiry;
    }

    public String getYourNameError() {
        return yourNameError;
    }

    public void setYourNameError(String yourNameError) {
        this.yourNameError = yourNameError;
    }

    public String getEmailError() {
        return emailError;
    }

    public void setEmailError(String emailError) {
        this.emailError = emailError;
    }

    public String getEnquiryError() {
        return enquiryError;
    }

    public void setEnquiryError(String enquiryError) {
        this.enquiryError = enquiryError;
    }

    public String getTypeOfTest() {
        return typeOfTest;
    }

    public void setTypeOfTest(String typeOfTest) {
        this.typeOfTest = typeOfTest;
    }

    public boolean expectSuccessfulContactUsRegistration() {
        if (!this.getYourNameError().trim().equalsIgnoreCase("")) {
            return false;

        }
        if (!this.getEmailError().trim().equalsIgnoreCase("")) {
            return false;
        }

        if (!this.getEnquiryError().trim().equalsIgnoreCase("")) {
            return false;
        }

        return true;
    }
}
