package ro.siit.tau.gr7.models;

public class RegistrationModel {

    private UserModel user;

    String email;
    String telephone;
    String password;
    String confirmPassword;

    String firstNameError;
    String lastNameError;
    String emailError;
    String telephoneError;
    String passwordError;
    String confirmPasswordError;
    String privacyPolicyWarning;

    String testType;

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getFirstNameError() {
        return firstNameError;
    }

    public void setFirstNameError(String firstNameError) {
        this.firstNameError = firstNameError;
    }

    public String getLastNameError() {
        return lastNameError;
    }

    public void setLastNameError(String lastNameError) {
        this.lastNameError = lastNameError;
    }

    public String getEmailError() {
        return emailError;
    }

    public void setEmailError(String emailError) {
        this.emailError = emailError;
    }

    public String getTelephoneError() {
        return telephoneError;
    }

    public void setTelephoneError(String telephoneError) {
        this.telephoneError = telephoneError;
    }

    public String getPasswordError() {
        return passwordError;
    }

    public void setPasswordError(String passwordError) {
        this.passwordError = passwordError;
    }

    public String getConfirmPasswordError() {
        return confirmPasswordError;
    }

    public void setConfirmPasswordError(String confirmPasswordError) {
        this.confirmPasswordError = confirmPasswordError;
    }

    public String getPrivacyPolicyWarning() {
        return privacyPolicyWarning;
    }

    public void setPrivacyPolicyWarning(String privacyPolicyWarning) {
        this.privacyPolicyWarning = privacyPolicyWarning;
    }

    public String getTestType() {
        return testType;
    }

    public void setTestType(String testType) {
        this.testType = testType;
    }

    public boolean expectSuccessfulRegistration() {
        if (!this.getFirstNameError().trim().equalsIgnoreCase("")) {
            return false;

        }
        if (!this.getLastNameError().trim().equalsIgnoreCase("")) {
            return false;
        }

        if (!this.getEmailError().trim().equalsIgnoreCase("")) {
            return false;

        }
        if (!this.getTelephoneError().trim().equalsIgnoreCase("")) {
            return false;
        }

        if (!this.getPasswordError().trim().equalsIgnoreCase("")) {
            return false;
        }

        if (!this.getConfirmPasswordError().trim().equalsIgnoreCase("")) {
            return false;
        }

        return true;
    }
}
