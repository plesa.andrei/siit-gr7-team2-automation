package ro.siit.tau.gr7.tests;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ro.siit.tau.gr7.models.ContactUsModel;
import ro.siit.tau.gr7.pages.ContactUsPage;
import ro.siit.tau.gr7.reader.ExcelReader;

import java.io.File;

public class ContactUsTest extends BaseTest {


    public void registerContactUs(ContactUsModel contactUsModel) {

        if (contactUsModel.expectSuccessfulContactUsRegistration()) {
            System.out.println("Contact Us registration with VALID credentials");
        } else {
            System.out.println("Contact Us registration with INVALID credentials");
        }

        driver.get("http://siit.epizy.com/shop/index.php?route=information/contact");

        contactUsPage = PageFactory.initElements(driver, ContactUsPage.class);
        contactUsPage.ContactUs(contactUsModel);

        if (contactUsModel.getTypeOfTest().toUpperCase().equals("NEGATIVE")) {

            if (isElementDisplayed(contactUsPage.getYourNameError())) {
                Assert.assertEquals(contactUsPage.getYourNameError().getText(), contactUsModel.getYourNameError(), "Checking Contact Us your name error(element is displayed in page) ");
            }

            if (isElementDisplayed(contactUsPage.getEmailError())) {
                Assert.assertEquals(contactUsPage.getEmailError().getText(), contactUsModel.getEmailError(), "Checking Contact Us email error(element is displayed in page) ");
            }

            if (isElementDisplayed(contactUsPage.getEnquiryError())) {
                Assert.assertEquals(contactUsPage.getEnquiryError().getText(), contactUsModel.getEnquiryError(), "Checking Contact Us enquiry error(element is displayed in page) ");
            }

        } else {

            Assert.assertEquals(contactUsPage.getSuccessfullCuntactUsForm().getText(), "Continue", "Check positive Contact Us form ");
        }
    }


    @DataProvider(name = "ExcelDataProvider")
    public Object[][] excelDataProviderTable() throws Exception {

        File excelFile = getFileFromResources("excel/ContactUsData.xlsx");
        String[][] excelData = ExcelReader.readExcelFile(excelFile, "Sheet1", true, true);
        Object[][] dp = new Object[excelData.length][1];

        for (int i = 0; i < dp.length; i++) {

            ContactUsModel contactUsModel = new ContactUsModel();


            contactUsModel.setYourName(excelData[i][0]);
            contactUsModel.setEmailAddress(excelData[i][1]);
            contactUsModel.setEnquiry(excelData[i][2]);

            contactUsModel.setYourNameError(excelData[i][3]);
            contactUsModel.setEmailError(excelData[i][4]);
            contactUsModel.setEnquiryError(excelData[i][5]);

            contactUsModel.setTypeOfTest(excelData[i][6]);


            dp[i][0] = contactUsModel;
        }
        return dp;
    }
    @Test(dataProvider = "ExcelDataProvider")
    public void contactUsTestWithExcelDP(ContactUsModel contactUsModel) {
        registerContactUs(contactUsModel);
    }

}

