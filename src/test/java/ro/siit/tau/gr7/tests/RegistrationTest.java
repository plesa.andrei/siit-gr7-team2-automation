package ro.siit.tau.gr7.tests;



import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ro.siit.tau.gr7.models.RegistrationModel;
import ro.siit.tau.gr7.models.UserModel;
import ro.siit.tau.gr7.pages.RegistrationPage;
import ro.siit.tau.gr7.reader.*;


import java.io.File;

public class RegistrationTest extends BaseTest {

    public void register(RegistrationModel registrationModel) {

        if (registrationModel.expectSuccessfulRegistration()) {
            System.out.println("Register with VALID credentials");
        } else {
            System.out.println("Register with INVALID credentials");
        }

        driver.get("http://siit.epizy.com/shop/index.php?route=account/register");

        registrationPage = PageFactory.initElements(driver, RegistrationPage.class);

        registrationPage.register(registrationModel);

        System.out.println("fn :" + registrationModel.getUser().getFirstName() + " fn error expected: "+registrationModel.getFirstNameError());


        if (registrationModel.getTestType().toUpperCase().equals("NEGATIVE")) {



            if (isElementDisplayed(registrationPage.getFirstNameError())) {
                Assert.assertEquals(registrationPage.getFirstNameError().getText(), registrationModel.getFirstNameError(), "Checking firstName error(elem. is displayed in page) ");
            } else {
                Assert.assertEquals("", registrationModel.getFirstNameError(), "Checking firstName error(elem. is not displayed in page)");
            }

            if (isElementDisplayed(registrationPage.getLastNameError())) {
                Assert.assertEquals(registrationPage.getLastNameError().getText(), registrationModel.getLastNameError(), "Checking lastName error(elem. is displayed in page)");
            } else {
                Assert.assertEquals("", registrationModel.getLastNameError(), "Checking lastName error(elem. is not displayed in page)");
            }

            if (isElementDisplayed(registrationPage.getEmailError())) {
                Assert.assertEquals(registrationPage.getEmailError().getText(), registrationModel.getEmailError(), "Checking email error(elem. is displayed in page)");
            } else {
                Assert.assertEquals("", registrationModel.getEmailError(), "Checking email error(elem. is not displayed in page)");
            }

            if (isElementDisplayed(registrationPage.getTelephoneError())) {
                Assert.assertEquals(registrationPage.getTelephoneError().getText(), registrationModel.getTelephoneError(), "Checking telephone error(elem. is displayed in page)");
            } else {
                Assert.assertEquals("", registrationModel.getTelephoneError(), "Checking telephone error(elem. is not displayed in page)");
            }

            if (isElementDisplayed(registrationPage.getPasswordError())) {
                Assert.assertEquals(registrationPage.getPasswordError().getText(), registrationModel.getPasswordError(), "Checking password error(elem. is displayed in page)");
            } else {
                Assert.assertEquals("", registrationModel.getPasswordError(), "Checking password error(elem. is not displayed in page)");
            }

            if (isElementDisplayed(registrationPage.getConfirmPasswordError())) {
                Assert.assertEquals(registrationPage.getConfirmPasswordError().getText(), registrationModel.getConfirmPasswordError(), "Checking confirm pass. error(elem. is displayed in page)");
            } else {
                Assert.assertEquals("", registrationModel.getConfirmPasswordError(), "Checking confirm pass. error(elem. is not displayed in page)");
            }

            if (isElementDisplayed(registrationPage.getPrivacyPolicyWarning())) {
                Assert.assertEquals(registrationPage.getPrivacyPolicyWarning().getText(), registrationModel.getPrivacyPolicyWarning(), "Checking privacy polici warning(elem. is displayed in page)");
            } else {
                Assert.assertEquals("", registrationModel.getConfirmPasswordError(), "Checking privacy polici warning(elem. is not displayed in page)");
            }


        } else {

            if (!registrationPage.getCheckBox().isSelected()) {
                registrationPage.getCheckBox().click();
            }

            registrationPage.getSubmitButton().submit();

            Assert.assertEquals(registrationPage.getPrivacyPolicyWarning().getText(),registrationModel.getPrivacyPolicyWarning(), "check pozitive register with preregistred e-mail");
        }
    }

    @DataProvider(name = "ExcelDataProvider")
    public Object[][] excelDataProviderTable() throws Exception {

        ClassLoader classLoader = getClass().getClassLoader();
        File excelFile = getFileFromResources("excel/RegisterData.xlsx");
        String[][] excelData = ExcelReader.readExcelFile(excelFile, "Sheet1", true, true);
        Object[][] dp = new Object[excelData.length][1];

        for (int i = 0; i < dp.length; i++) {

            RegistrationModel registrationModel = new RegistrationModel();

            UserModel user=new UserModel();

            user.setFirstName(excelData[i][0]);
            user.setLastName(excelData[i][1]);

            registrationModel.setUser(user);

            registrationModel.setEmail(excelData[i][2]);
            registrationModel.setTelephone(excelData[i][3]);

            registrationModel.setPassword(excelData[i][4]);
            registrationModel.setConfirmPassword(excelData[i][5]);

            registrationModel.setFirstNameError(excelData[i][6]);
            registrationModel.setLastNameError(excelData[i][7]);

            registrationModel.setEmailError(excelData[i][8]);
            registrationModel.setTelephoneError(excelData[i][9]);

            registrationModel.setPasswordError(excelData[i][10]);
            registrationModel.setConfirmPasswordError(excelData[i][11]);
            registrationModel.setPrivacyPolicyWarning(excelData[i][12]);

            registrationModel.setTestType(excelData[i][13]);

            dp[i][0] = registrationModel;
        }
        return dp;
    }

    @Test(dataProvider = "ExcelDataProvider")
    public void registerTestWithExcelDP(RegistrationModel registrationModel) {
        register(registrationModel);
    }
}
