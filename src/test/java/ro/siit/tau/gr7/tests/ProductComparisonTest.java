package ro.siit.tau.gr7.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.*;
import ro.siit.tau.gr7.pages.ProductComparisonPage;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ProductComparisonTest extends BaseTest {

    private ProductComparisonPage productComparisonPage;

    private static final int MAX_NUMBER_OF_PRODUCTS_TO_COMPARE = 4;

    private void insertProductsToCompare(int firstNProductsFinded) {

        driver.get("http://siit.epizy.com/shop/index.php?route=product/category&path=20");

        for (int i = 0; i < firstNProductsFinded; i++) {
            driver.navigate().refresh();

            List<WebElement> productsAdded = driver.findElements(By.xpath("//button[@data-original-title='Compare this Product']"));

            productsAdded.get(i).click();

            driver.findElement(By.cssSelector("[class*='alert alert-success ']"));
            driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        }
    }

    private List<String> getNameInsertedProductsToCompare(int numberOfProductsInserted) {

        List<WebElement> productsName = driver.findElements(By.xpath("//h4"));
        List<String> productNameText = new ArrayList<>();

        for (int i = 0; i < numberOfProductsInserted; i++) {
            productNameText.add(productsName.get(i).getText());
        }
        return productNameText;
    }

    private void removeFirstProductFromComparisonPage() {

        driver.get("http://siit.epizy.com/shop/index.php?route=product/compare");
        productComparisonPage = PageFactory.initElements(driver, ProductComparisonPage.class);

        if (productComparisonPage.getRemoveListElements().size() > 0) {

            productComparisonPage.getRemoveListElements().get(0).click();
        }
    }

    private void removeAllProductsFromComparisonPage() {

        driver.get("http://siit.epizy.com/shop/index.php?route=product/compare");
        productComparisonPage = PageFactory.initElements(driver, ProductComparisonPage.class);

        int size = productComparisonPage.getRemoveListElements().size();

        for (int i = 0; i < size; i++) {
            removeFirstProductFromComparisonPage();
        }
    }

    @DataProvider(name = "DataProviderForNumberOfProducts")
    public Object[] dataProvider() {

        Object[] dp = new Object[3];

        dp[0] = 2;
        dp[1] = 4;
        dp[2] = 8;

        return dp;
    }

    @BeforeMethod
    private void clearProductsFromComparisonPage() {
        removeAllProductsFromComparisonPage();
    }

    @Test(dataProvider = "DataProviderForNumberOfProducts")
    public void productComparisonTestForFirstNInsertedProducts(int numberOfProductsToInsert) {

        insertProductsToCompare(numberOfProductsToInsert);

        List<String> productNameText = getNameInsertedProductsToCompare(numberOfProductsToInsert);

        driver.get("http://siit.epizy.com/shop/index.php?route=product/compare");

        productComparisonPage = PageFactory.initElements(driver, ProductComparisonPage.class);

        if (numberOfProductsToInsert <= MAX_NUMBER_OF_PRODUCTS_TO_COMPARE) {

            for (int i = 0; i < numberOfProductsToInsert; i++) {
                Assert.assertEquals(productComparisonPage.getProductsListElements().get(i).getText(), productNameText.get(i), "compare products names added with the ones finded in comparison pagge");
            }
        } else {
            for (int i = 0; i < MAX_NUMBER_OF_PRODUCTS_TO_COMPARE; i++) {
                Assert.assertEquals(productComparisonPage.getProductsListElements().get(i).getText(),
                    productNameText.get(productNameText.size() - MAX_NUMBER_OF_PRODUCTS_TO_COMPARE + i),
                    "compare products names added with the ones finded in comparison pagge");
            }
        }
    }

    @Test
    public void productComparisonTestForRepeatedInsertionOfSameProduct() {

        insertProductsToCompare(1);
        insertProductsToCompare(1);//second add attempt of same product to comparison page
        insertProductsToCompare(1);// third add attempt of same product to comparison page

        List<String> productNameText = getNameInsertedProductsToCompare(1);

        driver.get("http://siit.epizy.com/shop/index.php?route=product/compare");

        productComparisonPage = PageFactory.initElements(driver, ProductComparisonPage.class);

        Assert.assertEquals(productComparisonPage.getProductsListElements().size(), 1, "check if product added repeatedly is only 1 time added in comparison page");

        Assert.assertEquals(productNameText.get(0), productComparisonPage.getProductsListElements().get(0).getText(), "check if the product added repeatedly is found in comparison page");

    }

    @Test
    public void removeElementFromComparisonPage() {

        insertProductsToCompare(5);

        removeFirstProductFromComparisonPage();
        removeFirstProductFromComparisonPage();
        removeFirstProductFromComparisonPage();
        removeFirstProductFromComparisonPage();


        driver.get("http://siit.epizy.com/shop/index.php?route=product/compare");

        productComparisonPage = PageFactory.initElements(driver, ProductComparisonPage.class);

        Assert.assertEquals(productComparisonPage.getProductsListElements().size(), 0, "check if the number of products removed is correct");

    }
}
