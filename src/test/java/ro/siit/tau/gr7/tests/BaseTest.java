package ro.siit.tau.gr7.tests;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import ro.siit.tau.gr7.pages.ContactUsPage;
import ro.siit.tau.gr7.pages.LoginPage;
import ro.siit.tau.gr7.pages.RegistrationPage;
import ro.siit.tau.gr7.pages.ReturnPage;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.concurrent.TimeUnit;

public class BaseTest {

    public WebDriver driver;
    public RegistrationPage registrationPage;
    public ReturnPage returnPage;
    public LoginPage loginPage;
    public ContactUsPage contactUsPage;

    @BeforeTest
    public void setUp() {

        driver = WebBrowser.getDriver(BrowserEnum.CHROME);

        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @AfterTest
    public void quitChrome() {

      driver.quit();

    }

    File[] getListOfFilesFromResources(String directoryName) throws UnsupportedEncodingException {
        ClassLoader classLoader = getClass().getClassLoader();
        File directory = new File(URLDecoder.decode(classLoader.getResource(directoryName).getPath(), "UTF-8"));
        File[] files = directory.listFiles();
        System.out.println("Found " + files.length + " files in " + directoryName + " folder");
        return files;
    }

    public File getFileFromResources(String fileName) throws UnsupportedEncodingException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(URLDecoder.decode(classLoader.getResource(fileName).getFile(), "UTF-8"));
        System.out.println(String.format("Found file %s", file.getName()));
        return file;
    }

    public boolean isElementDisplayed(WebElement element) {
        try {
            return element.isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }


}