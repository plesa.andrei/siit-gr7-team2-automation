package ro.siit.tau.gr7.tests;


import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ro.siit.tau.gr7.models.LoginModel;
import ro.siit.tau.gr7.pages.LoginPage;
import ro.siit.tau.gr7.reader.*;

import java.io.File;

public class LoginTest extends BaseTest{

    public void login(LoginModel loginModel) {

        driver.get("http://siit.epizy.com/index.php?route=account/login");

        loginPage = PageFactory.initElements(driver, LoginPage.class);

        loginPage.login(loginModel);

        System.out.println("email login :" + loginModel.getEmail() + " error found: " + loginModel.getEmailError());


        if (loginModel.getTypeOfTest().toUpperCase().equals("NEGATIVE")) {

            if (isElementDisplayed(loginPage.getWarningTextErrorElement())) {
                Assert.assertEquals(loginPage.getWarningTextErrorElement().getText(), loginModel.getEmailError(), "Checking warning text error(elem. is displayed in page) ");
            }

        } else {

            Assert.assertEquals(loginPage.getAccountLoginSuccesfulMessage().getText(), "My Account", "check pozitive login ");
        }
    }

    @DataProvider(name = "ExcelDataProvider")
    public Object[][] excelDataProviderTable() throws Exception {

        ClassLoader classLoader = getClass().getClassLoader();
        File excelFile = getFileFromResources("excel/LoginData.xlsx");
        String[][] excelData = ExcelReader.readExcelFile(excelFile, "Sheet1", true, true);
        Object[][] dp = new Object[excelData.length][1];

        for (int i = 0; i < dp.length; i++) {

            LoginModel loginModel = new LoginModel();


            loginModel.setEmail(excelData[i][0]);
            loginModel.setPassword(excelData[i][1]);
            loginModel.setEmailError(excelData[i][2]);

            loginModel.setTypeOfTest(excelData[i][3]);


            dp[i][0] = loginModel;
        }
        return dp;
    }

    @Test(dataProvider = "ExcelDataProvider")
    public void loginTestWithExcelDP(LoginModel loginModel) {
        login(loginModel);
    }

}
