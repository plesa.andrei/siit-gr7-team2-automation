package ro.siit.tau.gr7.tests;


import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ro.siit.tau.gr7.models.ReturnModel;
import ro.siit.tau.gr7.pages.ReturnPage;
import ro.siit.tau.gr7.reader.ExcelReader;

import java.io.File;

public class ReturnTest extends BaseTest {

    public void registerReturn(ReturnModel returnModel) {

        if (returnModel.expectSuccessfulRegister()) {
            System.out.println("Register with VALID credentials");
        } else {
            System.out.println("Register with INVALID credentials");
        }

        driver.get("http://siit.epizy.com/shop/index.php?route=account/return/add");

        returnPage = PageFactory.initElements(driver, ReturnPage.class);

        returnPage.register(returnModel);

        System.out.println("fn :" + returnModel.getFirstName() + " fn error expected: "+returnModel.getFirstNameError());


        if (returnModel.getTypeOfTest().toUpperCase().equals("NEGATIVE")) {

            if (isElementDisplayed(returnPage.getFirstNameError())) {
                Assert.assertEquals(returnPage.getFirstNameError().getText(), returnModel.getFirstNameError(), "Checking Returns firstName error(elem. is displayed in page) ");
            } else {
                Assert.assertEquals("", returnModel.getFirstNameError(), "Checking Returns firstName error(elem. is not displayed in page)");
            }

            if (isElementDisplayed(returnPage.getLastNameError())) {
                Assert.assertEquals(returnPage.getLastNameError().getText(), returnModel.getLastNameError(), "Checking Returns lastName error(elem. is displayed in page)");
            } else {
                Assert.assertEquals("", returnModel.getLastNameError(), "Checking Returns lastName error(elem. is not displayed in page)");
            }

            if (isElementDisplayed(returnPage.getEmailError())) {
                Assert.assertEquals(returnPage.getEmailError().getText(), returnModel.getEmailError(), "Checking Returns email error(elem. is displayed in page)");
            } else {
                Assert.assertEquals("", returnModel.getEmailError(), "Checking Returns email error(elem. is not displayed in page)");
            }

            if (isElementDisplayed(returnPage.getTelephoneError())) {
                Assert.assertEquals(returnPage.getTelephoneError().getText(), returnModel.getTelephoneError(), "Checking Returns telephone error(elem. is displayed in page)");
            } else {
                Assert.assertEquals("", returnModel.getTelephoneError(), "Checking Returns telephone error(elem. is not displayed in page)");
            }

            if (isElementDisplayed(returnPage.getOrderIDError())) {
                Assert.assertEquals(returnPage.getOrderIDError().getText(), returnModel.getOrderIDError(), "Checking Returns order ID  error(elem. is displayed in page)");
            } else {
                Assert.assertEquals("", returnModel.getOrderIDError(), "Checking Returns order ID error(elem. is not displayed in page)");
            }

            if (isElementDisplayed(returnPage.getProductNameError())) {
                Assert.assertEquals(returnPage.getProductNameError().getText(), returnModel.getProductNameError(), "Checking Returns product name error(elem. is displayed in page)");
            } else {
                Assert.assertEquals("", returnModel.getProductNameError(), "Checking Returns product name  error(elem. is not displayed in page)");
            }

            if (isElementDisplayed(returnPage.getProductCodeError())) {
                Assert.assertEquals(returnPage.getProductCodeError().getText(), returnModel.getProductCodeError(), "Checking Returns product code error(elem. is displayed in page)");
            } else {
                Assert.assertEquals("", returnModel.getProductCodeError(), "Checking Returns product code error(elem. is not displayed in page)");
            }

            if (isElementDisplayed(returnPage.getValueReasonBoxError())) {
                Assert.assertEquals(returnPage.getValueReasonBoxError().getText(), returnModel.getValueReasonBoxError(), "Checking Returns radio box error(elem. is displayed in page)");
            } else {
                Assert.assertEquals("", returnModel.getValueReasonBoxError(), "Checking Returns radio box error(elem. is not displayed in page)");
            }


        } else {

            Assert.assertEquals(returnPage.getConfirmationRequest().getText(), "You will be notified via e-mail as to the status of your request.", "check pozitive register for Returns");
        }
    }

    @DataProvider(name = "ExcelDataProvider")
    public Object[][] excelDataProviderTable() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File excelFile = getFileFromResources("excel/ReturnData.xlsx");
        String[][] excelData = ExcelReader.readExcelFile(excelFile, "Sheet1", true, true);
        Object[][] dp = new Object[excelData.length][1];

        for (int i = 0; i < dp.length; i++) {

            ReturnModel returnModel = new ReturnModel();

            returnModel.setFirstName(excelData[i][0]);
            returnModel.setLastName(excelData[i][1]);
            returnModel.setEmail(excelData[i][2]);
            returnModel.setTelephone(excelData[i][3]);

            returnModel.setOrderID(excelData[i][4]);
            returnModel.setOrderDate(excelData[i][5]);

            returnModel.setProductName(excelData[i][6]);
            returnModel.setProductCode(excelData[i][7]);
            returnModel.setQuantity(excelData[i][8]);
            returnModel.setValueReasonBox(excelData[i][9]);
            returnModel.setDetails(excelData[i][10]);

            returnModel.setFirstNameError(excelData[i][11]);
            returnModel.setLastNameError(excelData[i][12]);
            returnModel.setEmailError(excelData[i][13]);
            returnModel.setTelephoneError(excelData[i][14]);
            returnModel.setOrderIDError(excelData[i][15]);
            returnModel.setProductNameError(excelData[i][16]);
            returnModel.setProductCodeError(excelData[i][17]);
            returnModel.setValueReasonBoxError(excelData[i][18]);

            returnModel.setTypeOfTest(excelData[i][19]);

            dp[i][0] = returnModel;
        }
        return dp;
    }

    @Test(dataProvider = "ExcelDataProvider")
    public void returnTestWithExcelDP(ReturnModel returnModel) {
        registerReturn(returnModel);
    }
}
