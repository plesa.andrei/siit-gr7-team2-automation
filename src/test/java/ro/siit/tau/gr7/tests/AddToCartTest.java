package ro.siit.tau.gr7.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class AddToCartTest extends BaseTest {
        public void testAddToCart(int productsOnPage) {

            driver.get("http://siit.epizy.com/shop/index.php?route=product/category&path=24");

            for (int i = 0; i < productsOnPage; i++) {
               // driver.navigate().refresh();

                List<WebElement> productsAdded = driver.findElements(By.className("hidden-xs hidden-sm hidden-md"));

                productsAdded.get(i).click();

                driver.findElement(By.cssSelector("[class*='alert-success']"));
                driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

                for (WebElement webEl: productsAdded) {
                    System.out.println("product" + webEl.getAttribute("innerHTML"));
                }
            }



              /*  driver.get("http://siit.epizy.com/shop/");
                WebElement Phones = driver.findElement(By.linkText("Phones & PDAs"));
                Phones.click();
                WebDriverWait wait = new WebDriverWait(driver, 2);
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Phones & PDAs")));

                Assert.assertTrue(driver.getTitle().equals("Phones & PDAs"));

                driver.findElement(By.linkText("HTC Touch HD")).click();
                driver.findElement(By.id("button-cart")).click();

                driver.findElement(By.linkText("Phones & PDAs")).click();

                driver.findElement(By.linkText("iPhone")).click();
                driver.findElement(By.id("button-cart")).click();

                wait = new WebDriverWait(driver, 2);
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("cart-total")));

                driver.findElement(By.xpath("//*[@id=\"cart\"]/button")).click();

*/

        }
    @Test
    public void addProductsToCart(){
       testAddToCart(2);

    }
}
