package ro.siit.tau.gr7.tests;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ro.siit.tau.gr7.pages.LoginPage;
import ro.siit.tau.gr7.pages.ProductPageObj;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class AddToCartWishCompareTest extends BaseTest{

    @BeforeMethod
    private void initializateValidAccount(){
        driver.get("http://siit.epizy.com/shop/index.php?route=account/login");
        loginPage = PageFactory.initElements(driver, LoginPage.class);

        loginPage.login("plesa@andrei.com","123456");

    }


    @Test
    public void tesssst() throws InterruptedException {

        driver.get("http://siit.epizy.com/shop/index.php?route=product/category&path=24");

        ProductPageObj productPageObj=new ProductPageObj(driver);

        productPageObj.addToCart(1);
        String addToCartMessage =driver.findElement(By.cssSelector("[class*='alert-success']")).getText();
        Assert.assertTrue(addToCartMessage.contains("Success"));



        productPageObj.addToWishList("HTC Touch HD");
        String addToWhishListMessage =driver.findElement(By.cssSelector("[class*='alert-success']")).getText();
        Assert.assertTrue(addToWhishListMessage.contains("Success"));

        productPageObj.addToCompareList("HTC Touch HD");
        String addToCompareListMessage =driver.findElement(By.cssSelector("[class*='alert-success']")).getText();
        Assert.assertTrue(addToCompareListMessage.contains("Success"));


    }
}
