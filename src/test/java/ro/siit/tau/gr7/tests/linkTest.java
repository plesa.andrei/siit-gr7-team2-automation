package ro.siit.tau.gr7.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

public class linkTest {
    public WebDriver driver;

        @BeforeMethod
        public void setUp()
        {
            System.setProperty("webdriver.chrome.driver","src\\test\\resources\\drivers\\chromedriver.exe");
            driver= new ChromeDriver();
            driver.get("http://siit.epizy.com/shop/");
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }

      /*  @AfterMethod
        public void close()
        {
            driver.close();
        }
*/
        @DataProvider(name = "Links")
         public Iterator<Object[]> linksDataProvider() {
                Collection<Object[]> dp = new ArrayList<Object[]>();
                dp.add(new String[]{"Desktops","true"});
                dp.add(new String[]{"Laptops & Notebooks","true"});
                dp.add(new String[]{"Components","true"});
                dp.add(new String[]{"Tablets","false"});
                dp.add(new String[]{"Software","false"});
                dp.add(new String[]{"Phones & PDAs","false"});
                dp.add(new String[]{"Cameras","false"});
                dp.add(new String[]{"MP3 Players","true"});
                return dp.iterator();
            }

    @DataProvider(name = "productsDataProvider")
    public Iterator<Object[]> productsDataProvider() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
        dp.add(new String[]{"Canon EOS 5D"});
        /*dp.add(new String[]{"HP LP3065"});
        dp.add(new String[]{"MacBook"});
        dp.add(new String[]{"Sony VAIO"});
        dp.add(new String[]{"iPod Nano"});
        dp.add(new String[]{"iPod Shuffle"});
        dp.add(new String[]{"Samsung SyncMaster 941BW"});
        dp.add(new String[]{"Apple Cinema 30\""});*/
        return dp.iterator();
    }
            //test for link conections
            @Test(dataProvider = "Links")
            public void checkAllLinksTest(String categoryName, String listState)
            {
                WebElement category = driver.findElement(By.linkText(categoryName));
                if(listState.equals("true")) {
                    Actions actions = new Actions(driver);
                    actions.moveToElement(category).build().perform();
                    WebElement listcategory = driver.findElement(By.linkText("Show All " + categoryName));
                    listcategory.click();
                }
                else
                {
                    category.click();
                }
                WebElement categoryTitle = driver.findElement(By.tagName("h2"));

                Assert.assertEquals(categoryName,categoryTitle.getText());
            }

     //Test if search find all products from site
    @Test(dataProvider = "productsDataProvider")
    public void searchTest(String products)
    {
        WebElement  searchBox = driver.findElement(By.name("search"));
        searchBox.sendKeys(products);
        WebElement searchButton = driver.findElement(By.className("fa-search"));
        searchButton.click();
        WebElement verElement = driver.findElement(By.tagName("h4"));

        Assert.assertEquals(products,verElement.getText());
    }

}


