package ro.siit.tau.gr7.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ro.siit.tau.gr7.pages.LoginPage;
import ro.siit.tau.gr7.pages.ProductPageObj;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListGroupAccount extends BaseTest{

    @BeforeMethod
    private void initializateValidAccount(){
        driver.get("http://siit.epizy.com/shop/index.php?route=account/login");
        loginPage = PageFactory.initElements(driver, LoginPage.class);

        loginPage.login("plesa@andrei.com","123456");
       // driver.get("http://siit.epizy.com/shop/index.php?route=account/account");

    }

    @Test
    public void checkAllElementFromListGroup() {

        List<WebElement> listElements=driver.findElements(By.xpath("//aside/div/a"));

        List<String> innerHTMLList=new ArrayList<String>();
        List<String> hreList=new ArrayList<String>();


        for (WebElement web:listElements) {
            innerHTMLList.add(web.getAttribute("innerHTML"));
            hreList.add(web.getAttribute("href"));
        }

        for (int i = 0; i <hreList.size() ; i++) {
            driver.get(hreList.get(i));
        }

    }

    @Test
    public void tesssst() throws InterruptedException {

        driver.get("http://siit.epizy.com/shop/index.php?route=product/category&path=24");

        ProductPageObj productPageObj=new ProductPageObj(driver);

        productPageObj.addToCart("HTC Touch HD");
        String addToCartMessage =driver.findElement(By.cssSelector("[class*='alert-success']")).getText();
        Assert.assertTrue(addToCartMessage.contains("Success"));


        productPageObj.addToCart(2);
        String addToCartMessage1 =driver.findElement(By.cssSelector("[class*='alert-success']")).getText();
        Assert.assertTrue(addToCartMessage1.contains("Success"));



        productPageObj.addToWishList("HTC Touch HD");
        String addToWhishListMessage =driver.findElement(By.cssSelector("[class*='alert-success']")).getText();
        Assert.assertTrue(addToWhishListMessage.contains("Success"));

        productPageObj.addToCompareList("HTC Touch HD");
        String addToCompareListMessage =driver.findElement(By.cssSelector("[class*='alert-success']")).getText();
        Assert.assertTrue(addToCompareListMessage.contains("Success"));





//
//
//
//        for (WebElement we:productPageObj.getListAddToCartElement()
//             ) {
//            System.out.println(we.getText());
//
//        }
    }
}
