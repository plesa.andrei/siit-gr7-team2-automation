package ro.siit.tau.gr7.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;

public class ProductComparisonPage {


    @FindBy(how = How.CSS,using = "[class*='alert alert-success alert-dismissible']")
    private WebElement alertElement;

    @FindBy(how = How.XPATH,using = "//tr[td='Product']/td/a")
    private List<WebElement> productsListElements;

    @FindBy(how = How.CSS,using = "[class='btn btn-primary btn-block']")
    private List<WebElement> addChartListElements;

    @FindBy(how = How.CSS,using = "[class='btn btn-danger btn-block']")
    private List<WebElement> removeListElements;


    public WebElement getAlertElement() {
        return alertElement;
    }

    public List<WebElement> getProductsListElements() {
        return productsListElements;
    }

    public List<WebElement> getAddChartListElements() {
        return addChartListElements;
    }

    public List<WebElement> getRemoveListElements() {
        return removeListElements;
    }
}
