package ro.siit.tau.gr7.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class ProductPageObj {

    private WebDriver driver;

    public ProductPageObj(WebDriver driver) {
        this.driver = driver;
    }

    private WebElement getProductByName(String name) {
        return driver.findElement(By.xpath("//h4/a[text()='" + name + "']//ancestor::div[contains(@class,'product-layout')]"));
    }

    private WebElement getProductByName(int value) {
        try {
            return driver.findElement(By.xpath("(//h4/a//ancestor::div[contains(@class,'product-layout')])[" + value + "]"));
        } catch (org.openqa.selenium.NoSuchElementException e) {
            return driver.findElement(By.xpath("(//h4/a//ancestor::div[contains(@class,'product-layout')])[1]"));
        }
    }

    public void addToCart(String name) throws InterruptedException {
        WebElement product = getProductByName(name);
        WebElement addToCartBtn = product.findElement(By.xpath(".//i[contains(@class, 'fa-shopping-cart')]"));
        addToCartBtn.click();
        Thread.sleep(2000);

    }

    public void addToCart(int value) throws InterruptedException {
        WebElement product = getProductByName(value);
        WebElement addToCartBtn = product.findElement(By.xpath(".//i[contains(@class, 'fa-shopping-cart')]"));
        addToCartBtn.click();
        Thread.sleep(2000);

    }


    public void addToWishList(String name) throws InterruptedException {
        WebElement product = getProductByName(name);
        WebElement addToWishListBtn = product.findElement(By.xpath(".//i[contains(@class, 'fa-heart')]"));
        addToWishListBtn.click();
        Thread.sleep(2000);
    }

    public void addToCompareList(String name) throws InterruptedException {
        WebElement product = getProductByName(name);
        WebElement addToCompareListBtn = product.findElement(By.xpath(".//i[contains(@class, 'fa-exchange')]"));
        addToCompareListBtn.click();
        Thread.sleep(2000);

    }


//    @FindBy(how = How.XPATH,using = "//button[contains(@onclick,'cart.add')]")
//    private List<WebElement> listAddToCartElement;
//
////    @FindBy(how = How.XPATH,using = "(//button[contains(@onclick,'cart.add')])["+value+"]")
////    private List<WebElement> addToCartElement;
//
//    @FindBy(how = How.XPATH,using = "//button[contains(@onclick,'wishlist.add')]")
//    private List<WebElement> listAddToWishElement;
//
//
//    @FindBy(how = How.XPATH,using = "//button[contains(@onclick,'compare.add')]")
//    private List<WebElement> listAddToCompareElement;
//
//
//    @FindBy(how = How.XPATH,using = "//button[contains(@onclick,'compare.add')]")
//    private List<WebElement> listNameProductElement;
//
//    public List<WebElement> getListAddToCartElement() {return listAddToCartElement;}
//
//    public List<WebElement> getListAddToWishElement() {return listAddToWishElement;}
//
//    public List<WebElement> getListAddToCompareElement() {return listAddToCompareElement;}
//
//    public List<WebElement> getListNameProductElement() {return listNameProductElement;}
}
