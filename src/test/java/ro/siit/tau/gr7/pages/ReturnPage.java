package ro.siit.tau.gr7.pages;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import ro.siit.tau.gr7.models.ReturnModel;
import ro.siit.tau.gr7.tests.BaseTest;

import java.util.List;

public class ReturnPage extends BaseTest {

    @FindBy(how = How.ID, using = "input-firstname")
    private WebElement firstNameField;

    @FindBy(how = How.ID, using = "input-lastname")
    private WebElement lastNameField;

    @FindBy(how = How.ID, using = "input-email")
    private WebElement emailField;

    @FindBy(how = How.ID, using = "input-telephone")
    private WebElement telephoneField;

    @FindBy(how = How.ID, using = "input-order-id")
    private WebElement orderIDField;

    @FindBy(how = How.ID, using = "input-date-ordered")
    private WebElement orderDateField;

    @FindBy(how = How.ID, using = "input-product")
    private WebElement productNameField;

    @FindBy(how = How.ID, using = "input-model")
    private WebElement productCodeField;

    @FindBy(how = How.ID, using = "input-quantity")
    private WebElement quantityField;

    @FindBy(how = How.XPATH, using = "//input[@type='radio' and @name='return_reason_id']")
    private List<WebElement> listOfRadioBoxElements;

    @FindBy(how = How.ID, using = "input-comment")
    private WebElement detailsField;

    @FindBy(how = How.XPATH, using = "//div[@class='pull-right']/child::input")
    private WebElement submitButton;

    @FindBy(how = How.XPATH, using = "//div[@id='content']/child::p[2]")
    private WebElement confirmationRequest;




    @FindBy(how = How.XPATH, using = "//input[@id='input-firstname']/following-sibling::div")
    private WebElement firstNameError;

    @FindBy(how = How.XPATH, using = "//input[@id='input-lastname']/following-sibling::div")
    private WebElement lastNameError;

    @FindBy(how = How.XPATH, using = "//input[@id='input-email']/following-sibling::div")
    private WebElement emailError;

    @FindBy(how = How.XPATH, using = "//input[@id='input-telephone']/following-sibling::div")
    private WebElement telephoneError;

    @FindBy(how = How.XPATH, using = "//input[@id='input-order-id']/following-sibling::div")
    private WebElement orderIDError;

    @FindBy(how = How.XPATH, using = "//input[@id='input-product']/following-sibling::div")
    private WebElement productNameError;

    @FindBy(how = How.XPATH, using = "//input[@id='input-model']/following-sibling::div")
    private WebElement productCodeError;

    @FindBy(how = How.XPATH, using = "//input[@type='radio' and @name='return_reason_id' and @value='2']/parent::label/parent::div/following-sibling::div")
    private WebElement valueReasonBoxError;


    public void register(String firstName, String lastName, String email, String telephone,
                               String orderID, String orderDate,
                               String productName, String productCode, String quantity, String valueReasonBox, String details) {
        firstNameField.clear();
        firstNameField.sendKeys(firstName);

        lastNameField.clear();
        lastNameField.sendKeys(lastName);

        emailField.clear();
        emailField.sendKeys(email);

        telephoneField.clear();
        telephoneField.sendKeys(telephone);

        orderIDField.clear();
        orderIDField.sendKeys(orderID);

        orderDateField.clear();
        orderDateField.sendKeys(orderDate);

        productNameField.clear();
        productNameField.sendKeys(productName);

        productCodeField.clear();
        productCodeField.sendKeys(productCode);

        quantityField.clear();
        quantityField.sendKeys(quantity);

        int valueBox=-2;
        try {
             valueBox = Integer.parseInt(valueReasonBox);
        }catch (NumberFormatException e){
            valueBox=-1;
            System.out.println("value box must be integer type");
        }

        if( (valueBox>=0) && (valueBox<listOfRadioBoxElements.size()))  {
            if (isElementDisplayed(listOfRadioBoxElements.get(valueBox))) {
                listOfRadioBoxElements.get(valueBox).click();
            }
        }


        detailsField.clear();
        detailsField.sendKeys(details);

        submitButton.click();
    }

    public void register(ReturnModel returnModel){

        register(
            returnModel.getFirstName(),returnModel.getLastName(),returnModel.getEmail(),returnModel.getTelephone(),
            returnModel.getOrderID(),returnModel.getOrderDate(),
            returnModel.getProductName(),returnModel.getProductCode(),returnModel.getQuantity(),
            returnModel.getValueReasonBox(),returnModel.getDetails());
    }

    public WebElement getFirstNameError() {return firstNameError;}

    public WebElement getLastNameError() {return lastNameError;}

    public WebElement getEmailError() {return emailError;}

    public WebElement getTelephoneError() {return telephoneError;}

    public WebElement getOrderIDError() {return orderIDError;}

    public WebElement getProductNameError() {return productNameError;}

    public WebElement getProductCodeError() {return productCodeError;}

    public WebElement getValueReasonBoxError() {return valueReasonBoxError;}

    public WebElement getConfirmationRequest() {return confirmationRequest;}
}
