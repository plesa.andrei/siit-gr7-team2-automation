package ro.siit.tau.gr7.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import ro.siit.tau.gr7.models.LoginModel;

public class LoginPage {

    @FindBy(how = How.ID,using = "input-email")
    private WebElement emailElement;

    @FindBy(how = How.ID,using = "input-password")
    private WebElement passwordElement;

    @FindBy(how = How.CSS,using = "[class*='alert alert-danger']")//
    private WebElement warningTextErrorElement;

    @FindBy(how = How.XPATH,using = "//input[@type='submit']")//
    private WebElement loginButton;

    @FindBy(how = How.XPATH,using = "//div[@id='content']/h2[1]")//
    private WebElement accountLoginSuccesfulMessage;


    public void login(String email,String password)
    {
        emailElement.clear();
        emailElement.sendKeys(email);

        passwordElement.clear();
        passwordElement.sendKeys(password);

        loginButton.click();

    }

    public void login(LoginModel loginModel){
        login(loginModel.getEmail(),loginModel.getPassword());
    }

    public WebElement getEmailElement() {
        return emailElement;
    }

    public WebElement getPasswordElement() {
        return passwordElement;
    }

    public WebElement getWarningTextErrorElement() {return warningTextErrorElement;}

    public WebElement getAccountLoginSuccesfulMessage() {return accountLoginSuccesfulMessage;}
}
