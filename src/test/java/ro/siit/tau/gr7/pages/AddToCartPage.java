package ro.siit.tau.gr7.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class AddToCartPage {
    @FindBy(how = How.CLASS_NAME,using = "fa fa-shopping-cart")
    private WebElement addToCartElement;

    @FindBy(how = How.ID,using = "cart-total")
    private WebElement cartElement;

    @FindBy(how = How.CLASS_NAME,using = "btn btn-danger btn-xs")
    private WebElement removeElement;

    public WebElement getAddToCartElement() {
        return addToCartElement;
    }

    public WebElement getCartElement() {
        return cartElement;
    }

    public WebElement getRemoveElement() {
        return removeElement;
    }

    public void addProductToCart(String addToCart,String cartTotal, String removeProduct)
    {
        addToCartElement.click();
        cartElement.click();
        removeElement.click();

    }
}
