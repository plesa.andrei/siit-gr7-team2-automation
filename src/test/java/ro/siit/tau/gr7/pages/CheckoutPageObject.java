package ro.siit.tau.gr7.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class CheckoutPageObject {

    @FindBy(how = How.ID, using = "input-payment-firstname")
    private WebElement firstNameField;

    @FindBy(how = How.ID, using = "input-payment-lastname")
    private WebElement lastNameField;

    @FindBy(how = How.ID, using = "input-payment-address-1")
    private WebElement address1field;

    @FindBy(how = How.ID, using = "input-payment-city")
    private WebElement cityField;

    @FindBy(how = How.ID, using = "input-payment-country")
    private WebElement countryField;

    @FindBy(how = How.ID, using = "input-payment-zone")
    private WebElement regionField;

    @FindBy(how = How.ID, using = "button-payment-address")
    private WebElement billingContinueButton;

    @FindBy(how = How.ID, using = "button-shipping-address")
    private WebElement deliveryContinueButton;

    @FindBy(how = How.XPATH , using = "[@id=\"collapse-payment-method\"]/div/div[2]/div/input[1]")
    private  WebElement tAncCCheckbox;

    @FindBy(how = How.XPATH, using = "//*[@id=\"collapse-payment-method\"]/div/div[1]")
    private WebElement errorTermsAndConditions;

    @FindBy(how = How.ID, using = "button-confirm")
    private WebElement continueOrderButton;

}
