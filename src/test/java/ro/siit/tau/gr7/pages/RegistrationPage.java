package ro.siit.tau.gr7.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import ro.siit.tau.gr7.models.RegistrationModel;


public class RegistrationPage {

    @FindBy(how = How.ID,using = "input-firstname")
    private WebElement firstNameField;

    @FindBy(how = How.ID,using = "input-lastname")
    private WebElement lastNameField;

    @FindBy(how = How.ID,using = "input-email")
    private WebElement emailField;

    @FindBy(how = How.ID,using = "input-telephone")
    private WebElement telephoneField;

    @FindBy(how = How.ID,using = "input-password")
    private WebElement passwordField;

    @FindBy(how = How.ID,using = "input-confirm")
    private WebElement confirmPasswordField;

    @FindBy(how = How.XPATH,using = "//input[@type='submit']")
    private WebElement submitButton;

    @FindBy(how = How.XPATH,using = "//input[@type='radio' and @name='newsletter' and @value='1'] ")
    private WebElement radioButton;

    @FindBy(how = How.XPATH,using = "//input[@type='checkbox']")
    private WebElement checkBox;

    //

    @FindBy(how = How.XPATH,using = "//input[@id='input-firstname']/following-sibling::div")
    private WebElement firstNameError;

    @FindBy(how = How.XPATH,using = "//input[@id='input-lastname']/following-sibling::div")
    private WebElement lastNameError;

    @FindBy(how = How.XPATH,using = "//input[@id='input-email']/following-sibling::div")
    private WebElement emailError;

    @FindBy(how = How.XPATH,using = "//input[@id='input-telephone']/following-sibling::div")
    private WebElement telephoneError;

    @FindBy(how = How.XPATH,using = "//input[@id='input-password']/following-sibling::div")
    private WebElement passwordError;

    @FindBy(how = How.XPATH,using = "//input[@id='input-confirm']/following-sibling::div")
    private WebElement confirmPasswordError;

    @FindBy(how = How.CSS,using = "[class*='alert-danger']")//
    private WebElement privacyPolicyWarning;



    public void register(String firstName, String lastName,String email,String telephone,String password,String confirmPassword)
    {
        firstNameField.clear();
        firstNameField.sendKeys(firstName);

        lastNameField.clear();
        lastNameField.sendKeys(lastName);

        emailField.clear();
        emailField.sendKeys(email);

        telephoneField.clear();
        telephoneField.sendKeys(telephone);

        passwordField.clear();
        passwordField.sendKeys(password);

        confirmPasswordField.clear();
        confirmPasswordField.sendKeys(confirmPassword);

        radioButton.click();
        submitButton.click();

    }

    public void register(RegistrationModel registrationModel){
        register(
            registrationModel.getUser().getFirstName(),registrationModel.getUser().getLastName(),
            registrationModel.getEmail(),registrationModel.getTelephone(),
            registrationModel.getPassword(),registrationModel.getConfirmPassword());
    }

    public WebElement getFirstNameError() {
        return firstNameError;
    }

    public WebElement getLastNameError() {
        return lastNameError;
    }

    public WebElement getEmailError() {
        return emailError;
    }

    public WebElement getTelephoneError() {
        return telephoneError;
    }

    public WebElement getPasswordError() {
        return passwordError;
    }

    public WebElement getConfirmPasswordError() {
        return confirmPasswordError;
    }

    public WebElement getPrivacyPolicyWarning() {return privacyPolicyWarning;}

    public WebElement getSubmitButton() {return submitButton;}

    public WebElement getCheckBox() {return checkBox;}
}
