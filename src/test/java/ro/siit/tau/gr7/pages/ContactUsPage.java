package ro.siit.tau.gr7.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import ro.siit.tau.gr7.models.ContactUsModel;

public class ContactUsPage {
    @FindBy(how = How.ID, using = "input-name")
    private WebElement yourNameField;

    @FindBy(how = How.ID, using = "input-email")
    private WebElement emailAddressField;

    @FindBy(how = How.ID, using = "input-enquiry")
    private WebElement enquiryField;

    @FindBy(how = How.XPATH, using = "//div[@class='pull-right']/child::input")
    private WebElement submitButton;


    @FindBy(how = How.XPATH, using = "//input[@id='input-name']/following-sibling::div")
    private WebElement yourNameError;

    @FindBy(how = How.XPATH, using = "//input[@id='input-email']/following-sibling::div")
    private WebElement emailError;

    @FindBy(how = How.XPATH, using = "//input[@id='input-enquiry']/following-sibling::div")
    private WebElement enquiryError;

    @FindBy(how = How.LINK_TEXT, using = "Continue")
    private WebElement successfullCuntactUsForm;

    public void ContactUs(String yourName, String emailAddress, String enquiry) {
        yourNameField.clear();
        yourNameField.sendKeys(yourName);

        emailAddressField.clear();
        emailAddressField.sendKeys(emailAddress);

        enquiryField.clear();
        enquiryField.sendKeys(enquiry);

        submitButton.click();

    }

    public void ContactUs(ContactUsModel contactModel){
        ContactUs(contactModel.getYourName(), contactModel.getEmailAddress(), contactModel.getEnquiry());
    }

    public WebElement getYourNameError() {
        return yourNameError;
    }

    public WebElement getEmailError() {
        return emailError;
    }

    public WebElement getEnquiryError() {
        return enquiryError;
    }

    public WebElement getSuccessfullCuntactUsForm() {
        return successfullCuntactUsForm;
    }
}
